package com.telstra.codechallenge.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class HottestRepoService {

    @Value("${git.repo.url}")
    private String gitRepoUrl;

    @Autowired
    RestTemplate restTemplate;

    /**
     * Returns an array of git repositories from https://api.github.com/search/repositories.
     * @param count - number of repositories
     * @return - array of repositories
     */
    public ResponseEntity<String> getHottestRepos(int count) {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        String lastWeekDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());

        UriComponents uriBuilder = UriComponentsBuilder.fromHttpUrl(gitRepoUrl)
                .queryParam("q", "created:" + lastWeekDate)
                .queryParam("sort", "stars")
                .queryParam("order", "desc")
                .queryParam("per_page", count).build();

        ResponseEntity<String> responseEntity;
        try {
            responseEntity = restTemplate.getForEntity(uriBuilder.toString(), String.class);
        } catch (RestClientException e) {
            System.out.println("Exception while making query. Message = " + e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

}
