package com.telstra.codechallenge.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class HottestRepo {
    private List<RepoItem> items;
}
