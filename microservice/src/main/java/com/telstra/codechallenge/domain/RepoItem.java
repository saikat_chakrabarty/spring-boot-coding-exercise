package com.telstra.codechallenge.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class RepoItem {
    private String html_url;
    private int watchers_count;
    private String language;
    private String description;
    private String name;
}
