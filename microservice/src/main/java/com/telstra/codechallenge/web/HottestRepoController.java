package com.telstra.codechallenge.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telstra.codechallenge.domain.HottestRepo;
import com.telstra.codechallenge.service.HottestRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HottestRepoController {

    @Autowired
    HottestRepoService hottestRepoService;

    @Autowired
    ObjectMapper objectMapper;

    @RequestMapping(path = "/hottestrepo", method = RequestMethod.GET)
    public ResponseEntity<?> getHottestRepos(@RequestParam(value = "count", defaultValue = "1") int count) {

        if (count <= 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        ResponseEntity<String> response = hottestRepoService.getHottestRepos(count);
        HttpStatus statuscode = response.getStatusCode();
        String body = response.getBody();
        if (statuscode != HttpStatus.OK) {
            System.out.println("Query failed. Status code = " + statuscode);
            return new ResponseEntity<>("Query failed", statuscode);
        }

        HottestRepo hottestRepoResponse;
        try {
            hottestRepoResponse = objectMapper.readValue(body, HottestRepo.class);
        } catch (JsonProcessingException e) {
            System.out.println("Exception while parsing response. Message = " + e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<Object>(hottestRepoResponse, HttpStatus.OK);
    }

}
