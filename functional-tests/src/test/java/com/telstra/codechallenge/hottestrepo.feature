Feature: As an api user I want to retrieve some git repositories

  Scenario: Get 1 repository if queryparam is not provided
    Given url microserviceUrl
    And path '/hottestrepo'
    And param count 1
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # Define the required schema
    * def repoSchema = { items : { html_url : '#string', watchers_count : '#number', language : '#string', description : '#string',  name : '#string' } }
    # The response should have an array of 5 quote objects
    And match response == '#[1] repoSchema'

  Scenario: Get no repositories if queryparam is 0
    Given url microserviceUrl
    And path '/hottestrepo'
    And param count 0
    When method GET
    Then status 204
    And match header Content-Type contains 'application/json'
    And match response ==
    """
    """

  Scenario: Get 5 repositories if queryparam is 5
    Given url microserviceUrl
    And path '/hottestrepo'
    And param count 5
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # Define the required schema
    * def repoSchema = { items : { html_url : '#string', watchers_count : '#number', language : '#string', description : '#string',  name : '#string' } }
    # The response should have an array of 5 quote objects
    And match response == '#[5] repoSchema'
